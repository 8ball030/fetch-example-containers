"""
Not endorsed or supported by fetchai purely a community approach

"""

## Purpose

This repo shows how to set up both a fetch ai ledger and a fetch ai node with a containerised environment.


## Requirements
* docker, docker-compose


## Setup
```
git clone git clone https://8ball030@bitbucket.org/8ball030/fetch-example-containers.git
cd fetch-example-containers
docker-compose up
```

This takes quite a while to build...

## Notes

 * There is likely to versioning inconsistencies between this containerised version and the most recent release of fetch.

